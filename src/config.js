import {IdentityServiceSdkConfig} from 'identity-service-sdk';
import {SpiffApiGatewayServiceSdkConfig} from 'spiff-api-gateway-service-sdk';
import {ClaimSpiffServiceSdkConfig} from 'claim-spiff-service-sdk';
import {SessionManagerConfig} from 'session-manager';

export default class Config {

    _identityServiceSdkConfig:IdentityServiceSdkConfig;

    _spiffApiGatewaySdkConfig:SpiffApiGatewayServiceSdkConfig;

    _claimSpiffServiceSdkConfig:ClaimSpiffServiceSdkConfig;

    _sessionManagerConfig:SessionManagerConfig;


    /**
     * @param {IdentityServiceSdkConfig} identityServiceSdkConfig
     * @param {PartnerRepServiceSdkConfig} partnerRepServiceSdkConfig
     * @param {SessionManagerConfig} sessionManagerConfig
     */
    constructor(identityServiceSdkConfig:IdentityServiceSdkConfig,
                spiffApiGatewaySdkConfig:SpiffApiGatewayServiceSdkConfig,
                claimSpiffServiceSdkConfig:ClaimSpiffServiceSdkConfig,
                sessionManagerConfig:SessionManagerConfig) {

        if (!identityServiceSdkConfig) {
            throw new TypeError('identityServiceSdkConfig required');
        }
        this._identityServiceSdkConfig = identityServiceSdkConfig;

        if (!spiffApiGatewaySdkConfig) {
            throw new TypeError('spiffApiGatewaySdkConfig required');
        }
        this._spiffApiGatewaySdkConfig = spiffApiGatewaySdkConfig;

        if (!claimSpiffServiceSdkConfig) {
            throw new TypeError('claimSpiffServiceSdkConfig required');
        }
        this._claimSpiffServiceSdkConfig = claimSpiffServiceSdkConfig;

        if (!sessionManagerConfig) {
            throw new TypeError('sessionManagerConfig');
        }
        this._sessionManagerConfig = sessionManagerConfig;

    }

    get identityServiceSdkConfig() {
        return this._identityServiceSdkConfig;
    }

    get spiffApiGatewayServiceSdkConfig() {
        return this._spiffApiGatewaySdkConfig;
    }

    get claimSpiffServiceSdkConfig() {
        return this._claimSpiffServiceSdkConfig;
    }

    get sessionManagerConfig() {
        return this._sessionManagerConfig;
    }
}